"""
    pyAmap.gestionSqlite
    ~~~~~~~~~~~~~~~~~~~~

    Le module implémente la gestion de la base Sqlite.

    :copyright: (c) 2014 par Moulinux.
    :license: AGPLv3, voir la LICENCE pour plus d'informations.
"""
import sqlite3

DATABASE = 'pyAmap.db'
SCRIPT = 'pyAmap.sql'


class GestionBd:
    """Mise en place et interfaçage d'une base de données SQLite"""
    def __init__(self):
        """Établissement de la connexion - Création du curseur"""
        try:
            self.bdd = \
                sqlite3.connect(DATABASE,
                                check_same_thread=False,
                                detect_types=sqlite3.PARSE_DECLTYPES)
        except sqlite3.DatabaseError as err:
            print('La connexion avec la base de données a échoué :\n',
                  'Erreur détectée :{}'.format(err))
            self.echec = True
        else:
            self.bdd.row_factory = sqlite3.Row
            self.bdd.isolation_level = None    # autocommit
            self.curseur = self.bdd.cursor()   # création du curseur
            self.curseur.execute("PRAGMA foreign_keys=ON")
            self.echec = False

    def executer(self, req, params):
        """Exécution de la requête <req>, avec détection d'erreur éventuelle"""
        try:
            self.curseur.execute(req, params)
        except sqlite3.DatabaseError as err:
            # afficher la requête et le message d'erreur système :
            print("Requête SQL incorrecte :{}\nErreur détectée :{}"
                  .format(req, err))
            return False
        else:
            return True

    def obtenir_resultat(self):
        """
            renvoie le résultat de la requête précédente
            (une liste de sqlite3.Row)
        """
        return self.curseur.fetchall()

    def commit(self):
        """transfert du curseur vers le disque"""
        if self.bdd:
            self.bdd.commit()

    def fermer(self):
        """fermeture du curseur"""
        if self.bdd:
            self.curseur.close()
            self.bdd.close()

    def dernier_id(self):
        return self.curseur.lastrowid

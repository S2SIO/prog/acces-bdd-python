import sqlite3
import sys
import os

DATABASE = 'pyAmap.db'
SCRIPT = 'pyAmap.sql'

if not os.path.exists(DATABASE):
    with open(DATABASE, 'w'):
        print("Le fichier {} vient d'être créé".format(DATABASE))
        try:
            # on crée le fichier DATABASE dans le répertoire courant :
            bdd = sqlite3.connect(DATABASE)
            curseur = bdd.cursor()

            # on lit le fichier script SQL :
            with open(SCRIPT, 'r') as f:
                curseur.executescript(f.read())

            bdd.commit()
        except sqlite3.DatabaseError as err:
            print("Erreur {}".format(err))
            sys.exit(1)
        finally:
            if bdd:
                curseur.close()
                bdd.close()
else:
    print("Le fichier {} existe déjà".format(DATABASE))

import sqlite3
import sys

try:
    bdd = sqlite3.connect('pyAmap.db')
    curseur = bdd.cursor()

    # recherche de l'id du premier Paysan (optionnel)
    req = """SELECT nom FROM Personne
                WHERE id="""

    code = input("Saisissez l'identifiant de la personne recherchée : ")
    req = req + "'" + code + "';"
    # le programme peut être trompé en saisissant :
    # ' or 1 --
    curseur.execute(req)
    for p in curseur.fetchall():
        print(p[0])

except sqlite3.DatabaseError as e:
    print("Erreur {}".format(e))
    sys.exit(1)

finally:
    if bdd:
        curseur.close()
        bdd.close()

Voici plusieurs itérations d'une application permettant de démontrer
l'accès aux bases de données en Python. Afin de créer une base de données
SQLITE, vous pouvez exécuter le script `create_db.py`, prévu à cet effet.

Les programmes `demo_injection_sql_*.py` essaient de démontrer les
risques et les solutions en matière d'injection SQL...

## Initialisation de la base Sqlite

Il suffit de lancer le script `create_db.py` :
`python3 create_db.py`

## Démos

Plusieurs itérations permettent de mettre en œuvre l'accès à une base
de données sqlite... la plus simple (itération n°1) :
`python3 app_1.py`

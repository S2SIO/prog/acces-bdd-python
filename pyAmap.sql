DROP TABLE IF EXISTS Contrat;
DROP TABLE IF EXISTS Paysan;
DROP TABLE IF EXISTS Personne;

-- Création des tables
create table Personne 
(id char(5) not null,
nom varchar(45) not null,
prenom varchar(45) not null,
adresseRue varchar(45) not null, 
codePostal char(5) not null, 
ville varchar(35) not null,
telFixe varchar(10) default null,
telPortable varchar(10) default null,
adresseElectronique varchar(100) default null,
constraint pk_Personne primary key(id));

create table Paysan 
(idP char(5) not null,
culture varchar(15) not null,
nbPaniers integer not null,
presentation varchar(5000) default null,
constraint pk_Paysan primary key(idP),
constraint fk_Paysan foreign key(idP) references Personne(id));

create table Contrat
(idPaysan char(5) not null,
idMangeur char(5) not null,
debut date not null,
prixPanier integer not null,
nbSemaines integer not null,
constraint pk_Contrat primary key(idPaysan, idMangeur),
constraint fk1_Contrat foreign key(idPaysan) references Paysan(idP),
constraint fk2_Contrat foreign key(idMangeur) references Personne(id));

-- Les personnes sont fictives
insert into Personne values ('21001', 'Champrux', 'Dominique','Genestux', '63290', 'Puy-Guillaume', '0463841832', null,'dominique.champrux@amap-auvergne.org');
insert into Personne values ('21002', 'Moulinux', 'Stéphane','7 chemin des oiseaux', '03700', 'Bellerivux', '0493542678', null,'stephane.moulinux@amap-auvergne.org');
insert into Personne values ('21003', 'Salazux', 'Sabine','28 rue de la liberté', '63200', 'Riomux', '0473891566', null,'sabine.salazux@amap-auvergne.org');
insert into Personne values ('21004', 'Brerux', 'Hervé','Cayux', '03120', 'Droituriux', '0455667788', null,'herve.brerux@amap-auvergne.org');

-- Les paysans sont fictifs
insert into Paysan values ('21001', 'maraîchage', 80, 'je fais du maraîchage en biodynamie (Demeter) depuis 1991, sur 1,5ha. Mon exploitation est située à Puy-Guillaume à 20km environ de Vichy. Pour la fumure et le traitement des végétaux, j’ai un cahier des charges AB et utilise des préparations biodynamiques. Je suis à l’AMAP depuis novembre 2005 ce qui me permet de la régularité dans les ventes ; je viens toutes les semaines de mars à décembre. Je propose des paniers à 5,10,15,20,25€');
insert into Paysan values ('21004', 'porcs fermiers', 40, ' je produis des porcs fermiers de 10 mois d’âge et propose du porc frais et des produits de salaison. Ma ferme de 1O hectares est située au pied de la montagne Bourbonnaise dans les monts de La Madeleine entre Lapalisse et Arfeuilles à 25 km de Vichy. Je produis de manière extensive : mon cheptel comprend 1O truies conduites sur paille ; le sevrage des porcelets se fait à 60 jours au lieu de 24 jours en système intensif. Leur alimentation ainsi que celle des porcs est à base de farine dés le plus jeune âge jusqu’à l’abattage. Ces farines sont fabriquées à partir de céréales bio par un moulin local. Elevés sur paille de la naissance à 80 Kg nos cochons s’ébattent ensuite pendant 4 mois sur un parcours de 2 ha avant de rejoindre l’étable sur paille pour la finition à base de farine et de pommes de terre. Mon but : un cochon de 10 à 15 mois d’âge pesant de 150 à 250 kg. Toute notre production est vendue en direct sur les marchés locaux et bien sûr, depuis Septembre 2005 à l’AMAP de Vichy (tous les jeudis) ce qui représente aujourd’hui 10% de nos ventes. L’AMAP, dans son fonctionnement de type associatif, reliant les consommateurs aux producteurs correspond pleinement à mes attentes : cela me permet un rapprochement avec les consommateurs (échange de point de vue sur les problèmes et les attentes de chacun. Leur soutien permet la sauvegarde de la ruralité, des productions agricoles locales. Je souhaite à l’avenir bien définir ce qu’est la qualité du produit pour en assurer son développement dans un contexte respectueux de l’environnement.');

-- Les contrats sont fictifs
insert into Contrat values ('21001', '21002', '2021-01-07', 15, 20);
insert into Contrat values ('21001', '21003', '2021-01-14', 10, 10);

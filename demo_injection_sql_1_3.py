import sqlite3
import sys

bdd = None

try:
    bdd = sqlite3.connect('pyAmap.db')
    curseur = bdd.cursor()

    # rechercher par code postal
    req = """SELECT nom FROM Personne
                WHERE codePostal LIKE ?"""

    code = input("Saisissez le code postal des personnes recherchées : ")
    curseur.execute(req, (code + '%',))
    for p in curseur.fetchall():
        print(p[0])

except sqlite3.DatabaseError as e:
    print("Erreur {}".format(e))
    sys.exit(1)

finally:
    if bdd:
        curseur.close()
        bdd.close()

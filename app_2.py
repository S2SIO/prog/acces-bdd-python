import sqlite3
import sys

bdd = None

try:
    bdd = sqlite3.connect('pyAmap.db')
    curseur = bdd.cursor()

    req = """SELECT * FROM Paysan"""
    curseur.execute(req)

    print("Résultats (liste des cultures) :")
    # for ligne in curseur.fetchall():
    for ligne in curseur:
        print(" - {}".format(ligne[1]))

except sqlite3.DatabaseError as e:
    print("Erreur {}".format(e))
    sys.exit(1)

finally:
    if bdd:
        curseur.close()
        bdd.close()
